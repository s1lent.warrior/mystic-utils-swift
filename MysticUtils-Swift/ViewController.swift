//
//  ViewController.swift
//  MysticUtils-Swift
//
//  Created by Muneeb Ahmed Anwar on 09/10/2016.
//  Copyright © 2016 MysticBots. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet var progressBar: MBGradientProgressBar!
    
    lazy var allColors: [Any] = {
        var colors : [Any] = []
        
        for hue : CGFloat in stride(from: 0.0, to: 1.0, by: 0.01) {
            let color = UIColor(progress: hue)
            colors.append(color)
        }
        
        return colors
    }()
    
    var randomColor: UIColor {
        let index = Int.random(self.allColors.count)
        return self.allColors[index] as! UIColor
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    
        if #available(iOS 10.0, *) {
            let _ = Timer.scheduledTimer(withTimeInterval: 0.1, repeats: true) { (timer) in
                DispatchQueue.main.async {
                    self.progressBar.progress += 0.05
                    
                    var colors = self.progressBar.colors
                    if self.progressBar.progress >= 1.0 {
                        colors.removeLast()
                    }
                    let color = self.randomColor
                    colors.insert(color.cgColor, at: 0)
                    self.progressBar.setColors(colors: colors)
                }
            }
        } else {
            // Fallback on earlier versions
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

