//
//  MBAudioPlayer.swift
//
//
//  Created by Muneeb Ahmed Anwar on 07/10/2016.
//  Copyright © 2016 MysticBots. All rights reserved.
//

import AVFoundation

class MBAudioPlayer: NSObject {
    
    // MARK: - private properties
    fileprivate let audioQueue = DispatchQueue(label: UUID().uuidString)
    fileprivate let audioFile : AVAudioFile!
    fileprivate(set) var audioEngine : AVAudioEngine!
    fileprivate(set) var audioPlayerNode : AVAudioPlayerNode!
    
    fileprivate var audioPitchEffect = AVAudioUnitTimePitch() {
        didSet {
            self.audioPitchEffect.pitch = self.pitch
        }
    }
    fileprivate var localNumberOfLoops : Int = 0
    fileprivate var infinitePlay : Bool = false
    
    fileprivate var shouldPlay : Bool! {
        didSet {
            self.togglePlay() // calling on value change to change player state
        }
    }
    
    // MARK: - Public (Read Only) Properties
    var isReady : Bool {
        return self.audioEngine?.isRunning == true
    }
    
    var isPlaying : Bool {
        return self.audioPlayerNode?.isPlaying == true
    }
    
    fileprivate(set) var isPaused : Bool = true {
        didSet {
            if self.audioEngine?.isRunning == true {
                self.audioPlayerNode?.pause()
            }
        }
    }
    
    fileprivate(set) var isStopped : Bool = true {
        didSet {
            self.shouldPlay = false
        }
    }
    
    // MARK: - Public Properties
    var sourceUrl : URL
    weak var delegate : MBAudioPlayerDelegate?
    
    var numberOfLoops : Int! {
        didSet {
            self.localNumberOfLoops = numberOfLoops
            self.infinitePlay = (numberOfLoops < 0)
        }
    }
    
    var volume : Float! {
        didSet {
            self.audioEngine?.mainMixerNode.outputVolume = self.volume
        }
    }
    
    var pitch : Float! {
        didSet {
            self.audioPitchEffect.pitch = pitch
        }
    }
    
    // MARK: - initializers
    convenience init(_ sourceUrl: URL) throws {
        try self.init(sourceUrl, delegate: nil)
    }
    
    init(_ sourceUrl: URL, delegate: MBAudioPlayerDelegate?) throws {
        
        self.numberOfLoops = 0
        self.volume = 1.0
        self.pitch  = 1.0
        
        self.audioFile = try AVAudioFile(forReading: sourceUrl)
        
        self.sourceUrl = sourceUrl
        self.delegate = delegate
        
        super.init()
        
        try? self.prepare()
        
        NotificationCenter.default.addObserver(self, selector: #selector(handleInterruption(_:)), name: NSNotification.Name.AVAudioSessionInterruption, object: AVAudioSession.sharedInstance())
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
        
        try? AVAudioSession.sharedInstance().setActive(false)
    }
    
    // MARK: - Public Methods
    func play() {
        
        audioQueue.async {
            if self.audioPlayerNode?.isPlaying == false {
                do {
                    try self.prepare()
                    if self.audioEngine?.isRunning == true {
                        if self.isPaused {
                            self.isPaused = false
                        }
                        self.shouldPlay = true
                    }
                } catch let error as NSError {
                    print(error)
                } catch let error as NSException {
                    print(error)
                }
            }
        }
    }
    
    func pause() {
        audioQueue.async {
            if self.audioEngine?.isRunning == true {
                self.isPaused = true
            }
        }
    }
    
    func stop() {
        audioQueue.async {
            if self.audioEngine?.isRunning == true {
                self.isStopped = true
            }
        }
    }
    
    func prepare() throws {
        if isReady == false {
            if audioEngine != nil {
                sleep(1)
            }
            
            audioEngine = AVAudioEngine()
            
            audioPitchEffect = AVAudioUnitTimePitch()
            audioPlayerNode = AVAudioPlayerNode()
            audioEngine.mainMixerNode.outputVolume = self.volume
            
            //create audio engine and attach pitch-effect and player
            audioEngine.attach(audioPitchEffect)
            audioEngine.attach(audioPlayerNode)
            
            audioEngine.connect(audioPlayerNode, to: audioPitchEffect, format: nil)
            audioEngine.connect(audioPitchEffect, to: audioEngine.mainMixerNode, format: nil)
            audioEngine.connect(audioEngine.mainMixerNode, to: audioEngine.outputNode, format: nil)
            
            
            self.audioEngine.prepare()
            
            // start audio engine
            try self.audioEngine.start()
        }
    }
    
    // MARK: - Private Helper Methods
    @objc fileprivate func handleInterruption(_ notification: Notification) {
        if let interruptionType = notification.userInfo?[AVAudioSessionInterruptionTypeKey] {
            let type = AVAudioSessionInterruptionType(rawValue: interruptionType as! UInt)
            if type == .began {
                self.pause()
                try? AVAudioSession.sharedInstance().setActive(false)
                self.delegate?.audioPlayerBeginInterruption?(self)
            } else if type == .ended {
                self.play()
                try? AVAudioSession.sharedInstance().setActive(true)
                self.delegate?.audioPlayerEndInterruption?(self)
            }
        }
        
    }
    
    fileprivate func didFinishLoop() {
        if self.isStopped == false {
            var loopCount = self.numberOfLoops - self.localNumberOfLoops
            if self.infinitePlay == true {
                loopCount = (self.localNumberOfLoops * -1) - 1
            }
            
            self.delegate?.audioPlayerDidBeginLoop?(self, loopNumber: loopCount)
        }
    }
    
    fileprivate func togglePlay() {
        audioQueue.async {
            if self.shouldPlay == true {
                try? AVAudioSession.sharedInstance().setActive(true)
                if self.audioEngine?.isRunning == true {
                    self.audioPlayerNode.scheduleFile(self.audioFile, at: nil) {
                        self.localNumberOfLoops -= 1
                        if self.infinitePlay == true || self.localNumberOfLoops >= 0 {
                            self.didFinishLoop() //
                            self.togglePlay() // calling recursively to infinitely play this audio
                        } else {    // loops < 0 finish playing
                            self.shouldPlay = false
                            self.delegate?.audioPlayerDidFinishPlaying?(self)
                        }
                    }
                    self.audioPlayerNode.play()
                }
            } else {
                if self.audioEngine?.isRunning == true {
                    self.audioPlayerNode.stop()
                    self.audioEngine.stop()
                    self.audioEngine = nil
                    try? AVAudioSession.sharedInstance().setActive(false)
                }
                self.localNumberOfLoops = self.numberOfLoops
            }
        }
    }
}

@objc protocol MBAudioPlayerDelegate {
    @objc optional func audioPlayerBeginInterruption(_ audioPlayer: MBAudioPlayer)
    @objc optional func audioPlayerEndInterruption(_ audioPlayer: MBAudioPlayer)
    @objc optional func audioPlayerDidBeginLoop(_ audioPlayer: MBAudioPlayer, loopNumber loop: Int)
    @objc optional func audioPlayerDidFinishPlaying(_ audioPlayer: MBAudioPlayer)
}
