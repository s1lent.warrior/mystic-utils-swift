//
//  UIColor.swift
//  iFocusTour
//
//  Created by Muneeb Ahmed Anwar on 20/09/2016.
//  Copyright © 2016 MysticBots. All rights reserved.
//

import UIKit

public extension UIColor {
    
    // MARK: - Class Initializers
    public convenience init(progress : CGFloat) {
        self.init(hue: progress * 0.4, saturation: 0.9, brightness: 0.9, alpha: 1.0)
    }
    
    // MARK: - Class Methods
    public static func color(progress : CGFloat) -> UIColor {
        return UIColor(progress: progress)
    }
    
    // MARK: - Instance Methods
    func rgb() -> (red:Int, green:Int, blue:Int, alpha:Int)? {
        var fRed : CGFloat = 0
        var fGreen : CGFloat = 0
        var fBlue : CGFloat = 0
        var fAlpha: CGFloat = 0
        if self.getRed(&fRed, green: &fGreen, blue: &fBlue, alpha: &fAlpha) {
            let iRed = Int(fRed * 255.0)
            let iGreen = Int(fGreen * 255.0)
            let iBlue = Int(fBlue * 255.0)
            let iAlpha = Int(fAlpha * 255.0)
            
            return (red:iRed, green:iGreen, blue:iBlue, alpha:iAlpha)
        } else {
            // Could not extract RGBA components:
            return nil
        }
    }
}
