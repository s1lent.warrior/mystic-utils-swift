//
//  UIScrollView.swift
//  MysticExtensions
//
//  Created by Muneeb Ahmed Anwar on 18/09/2016.
//  Copyright © 2016 MysticBots. All rights reserved.
//

import UIKit

public extension UIScrollView {
    
    public func toImage() -> UIImage {
        var imageFromScrollView : UIImage
        
        UIGraphicsBeginImageContext(self.contentSize)
        
        let savedContentOffset = self.contentOffset
        let savedFrame = self.frame
        self.contentOffset = CGPoint.zero
        self.frame = CGRect(x: 0, y: 0, width: self.contentSize.width, height: self.contentSize.height)
        self.layer.render(in: UIGraphicsGetCurrentContext()!)
        
        imageFromScrollView = UIGraphicsGetImageFromCurrentImageContext()!
        
        self.contentOffset = savedContentOffset
        self.frame = savedFrame
        
        UIGraphicsEndImageContext()
        
        return imageFromScrollView
    }

}
