//
//  FileManager.swift
//  MysticExtensions
//
//  Created by Muneeb Ahmed Anwar on 18/09/2016.
//  Copyright © 2016 MysticBots. All rights reserved.
//

import Foundation

public extension FileManager {
    
    // MARK: - Class Methods
    public static func delete(_ fileInDocumentsDirectoryWithName: String, ofType fileType: String) throws {
        let docsPath : NSString = NSString(string:"~/Documents").expandingTildeInPath as NSString
        let filePath = docsPath.appendingPathComponent(fileInDocumentsDirectoryWithName) + ".\(fileType)"
        let fileManager = FileManager.default
        if (fileManager.fileExists(atPath: filePath)) {
            try fileManager.removeItem(atPath: filePath)
        }
    }
}
