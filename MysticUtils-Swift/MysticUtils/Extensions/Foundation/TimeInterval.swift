//
//  TimeInterval.swift
//  MysticExtensions
//
//  Created by Muneeb Ahmed Anwar on 18/09/2016.
//  Copyright © 2016 MysticBots. All rights reserved.
//

import Foundation

public extension TimeInterval {
    
    // MARK: - Instance Methods
    func timeString(_ showHrs: Bool) -> String {
        let dateFormatter = DateFormatter()
        if (showHrs) {
            dateFormatter.dateFormat = "HH:mm:ss"
        } else {
            dateFormatter.dateFormat = "mm:ss"
        }
        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        
        let timerDate = Date(timeIntervalSince1970: self)
        return dateFormatter.string(from: timerDate)
    }
}
